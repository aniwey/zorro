# README #

## Quick description ##

"zorro" is a personal C++ game project that features dirt, stone, water, bushes that grow fruits, and a simple but sophisticated physics engine.

It uses advanced C++11 features, uses boost serialization for saving/loading and SFML for rendering. It should work correctly on Win/Mac/Linux.

Controls are explained ingame. Don't hesitate to take a look at the code, that's the most interesting part :)

## How to compile and launch the game ##

You'll need CMake, gcc, the boost serialization module and SFML 2.

Once you've installed all that, go to the project directory and compile :

```
mkdir build ; cd build
cmake ..
make
```

Then, launch the game with :
```
cd .. ; cd bin
./zorro
```

**Don't forget to set your resolution in *config/cfg.txt*! :)**

## Interesting algorithms ##

Big ones :

* The whole gravity (and groups gravity) system in *src/Land_loopPixels.cpp*. Bushes leaves are stuck together, forming a group of pixels that can only fall together. This creates quite complex situations of gravity dependencies (i.e. group A can only fall if group B and C can fall and group C can fall if... etc), thus the need for this complex gravity system.
* Water physics system in *src/Land_loopWater.cpp*

Small ones :

* Dirt falling system in *src/Land_loopDirt.cpp*
* Fruit ripening system in *src/Fruit.cpp*
* Pathfinding in *src/Land_pathfindingAlgorithms.cpp*