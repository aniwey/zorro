#include "Color.hpp"

Color::Color(){
  r = g = b = 0;
}

Color::Color(int r, int g, int b){
  this->r = r;
  this->g = g;
  this->b = b;
}

sf::Color Color::getSfColor(){
  return sf::Color(r, g, b);
}
