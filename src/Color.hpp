#ifndef HPP_COLOR
#define HPP_COLOR

#include <boost/serialization/access.hpp>

#include <SFML/Graphics.hpp>

class Color{
  public:
    Color();
    Color(int, int, int);
    
    // Getters
    int getR(){ return r; }
    int getG(){ return g; }
    int getB(){ return b; }
    
    sf::Color getSfColor();
    
  private:
    // Serialization
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int){
      ar & r;
      ar & g;
      ar & b;
    }
  
    // Variables
    int r, g, b;
};

#endif
