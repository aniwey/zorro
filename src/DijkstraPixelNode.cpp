#include "DijkstraPixelNode.hpp"

DijkstraPixelNode::DijkstraPixelNode(int score){
  setParentAndScore(PathfindingPixelNodeCoordinates(-1, -1), score);
}

DijkstraPixelNode::DijkstraPixelNode(PathfindingPixelNodeCoordinates _parent, int _score){
  setParentAndScore(_parent, _score);
}

void DijkstraPixelNode::setParentAndScore(PathfindingPixelNodeCoordinates _parent, int _score){
  // Set the parent
  parent = _parent;
  
  // Set the score
  score = _score;
}
