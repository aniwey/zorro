#ifndef HPP_DijkstraPixelNode
#define HPP_DijkstraPixelNode

#include <map>

#include <boost/shared_ptr.hpp>

#include "PathfindingPixelNodeCoordinates.hpp"

class DijkstraPixelNode{
  public:
    DijkstraPixelNode(){ }
    DijkstraPixelNode(int); // Constructor used to create a new node without parent
    DijkstraPixelNode(PathfindingPixelNodeCoordinates, int); // Constructor used to create a new node with all parameters
    
    // Method used to update the node parent and score
    void setParentAndScore(PathfindingPixelNodeCoordinates, int);
      
    // The node parent
    PathfindingPixelNodeCoordinates parent;
      
    // The score
    int score;
};

#endif
