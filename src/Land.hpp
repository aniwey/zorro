#ifndef HPP_LAND
#define HPP_LAND

#include <iostream>
#include <list>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <SFML/Graphics.hpp>

#include "PathfindingPixelNodeCoordinates.hpp"
#include "DijkstraPixelNode.hpp"
#include "Pixel.hpp"

#include "algo.hpp"
#include "pixelType.hpp"

class Pixel;

// Enumeration used for the return value of all A* algorithms that must stop on a specific kind of pixels
typedef enum{
  NO_PATH_FOUND, // Returned if the algorithm didn't find any path
  PATH_FOUND_WITHOUT_STOP, // Returned if the algorithm found a path and stopped on the specific kind of pixels
  PATH_FOUND_WITH_STOP // Returned if the algorithm found a path without stopping
}aStarStopOnReturnValue;

class Land{
  public:
    Land();
    
    // Initialization
    void init(int, int);
    void newGame(int, int);
    
    // Loop
    void loop();
    
    // Writing pixels to the land
    void writePixelRectangle(int x, int y, int w, int h, pixelType type);
    void writeEverythingBetweenTwoOrientedIdenticalSquares(int x1, int y1, int x2, int y2, int size, pixelType type);
    void writePixelLine(int x1, int y1, int x2, int y2, pixelType type);
    void writeSinglePixel(int x, int y, pixelType type);
    
    // Pixels
    std::vector<std::vector<Pixel> > p; // Pixels array
    
    // Groups
    std::list<boost::shared_ptr<Group> > g; // Groups list
    boost::shared_ptr<Group> getPixelGroup(); // Create a new group and return a pointer to it
    
    // Entities
    std::list<boost::weak_ptr<Entity> > entities; // List of entities (weak_ptr, see boost::weak_ptr)
    void registerEntity(boost::shared_ptr<Entity>); // Register a new entities to the entities list using a shared_ptr to an entity, the shared_ptr probably being contained in a pixel
    
    // Updating
    std::vector<std::list<std::pair<int, int> > > atu; // Areas to update used during the pixels loop
    std::vector<std::list<std::pair<int, int> > > atuNotif; // Areas to update used for notifications
    
    void handleNotifications();
      void notifyWaterSurfaces();
      void switchAtuAndAtuNotif();
    
    void notifyForUpdatingThisRectangle(int, int, int, int);
    void notifyForUpdatingAroundThisPixel(int, int);
    void notifyForUpdatingThisPixel(int, int);
    void notifyEverything();
    
    // Various tests on pixels
    std::vector<pixelGravity> pixelGravityVector;
    std::vector<pixelPhysicalState> pixelPhysicalStateVector;
    std::vector<int> pixelGaseousOrLiquidPenetrationVector;
    bool aPixelOfThisGroupIsAdjacentToThisOne(int x, int y, boost::weak_ptr<Group> group);
    bool thisPixelExists(int x, int y);
    int howManyPixelsOfThisTypeAndThisGroupInThisRectangle(pixelType type, boost::weak_ptr<Group> group, int x1, int y1, int x2, int y2);
    int howManyPixelsOfThisTypeInThisRectangle(pixelType type, int x1, int y1, int x2, int y2);
    
    // Frame id
    int frame_id;
    
    // Size of the pixel arrays
    int width, height;
    
    // Serialization
    void saveGame(std::string);
    void loadGame(std::string);
    
    // Bool to redraw everything
    bool redrawEverything;

  private:
    // loop() steps
    void loopPixels();
      // loopPixels() functions
      void checkForGroupsSplittingAndUpdateDependencies();
      void resolveDependenciesBetweenGroups();
      void applyGravityOnAtu();
      void applyGravityOnGroupsOutsideAtu();
      void loopDirt();
      void loopWater();
      void destroyEmptyGroups();
    void loopEntities();
    
    // Falling functions used by loopPixels functions
    bool tryToMakeFallAlongWithPixelsBelowOrAbove(int, int);
    bool tryToMakeFallAlongWithPixelsBelowOrAbove(int, int, int&, int&);
    void makeFallThroughAGaseousOrLiquidPixel(int x, int y); // Called when we try to make fall one pixel only 
    void makeFallThroughAGaseousOrLiquidPixel(int x, int y1, int y2); // Called when we try to make fall a whole column of pixels
    void makeFall(int, int);
    void makeFall(int, int, int, int);
    
    // Water functions used by loopWater()
    bool tryToPushThisWaterPixel(int, int);
    bool tryToPushThisWaterPixelFromHere(int, int, int);
    void tryToPushThisWaterPixelToTheCenterOfItsWaterBody(int, int);
    void findWaterBodyLimits(int, int, int&, int&, int = 1); // Find the left and right x limits of a water body
    bool findWaterBodyLimitsAndReturnTrueIfThereIsAnyVoidPixelAboveTheBody(int, int, int&, int&, int = 1);
    
    // Pathfinding related methods
      // Generic methods
      std::list<PathfindingPixelNodeCoordinates> getPathfindingNeighborNodesCoordinates(PathfindingPixelNodeCoordinates);
      // Methods used when pixels have to fall through a gaseous or liquid pixel
      aStarStopOnReturnValue pathfindingThroughGaseousOrLiquidStopOnVoid(PathfindingPixelNodeCoordinates, PathfindingPixelNodeCoordinates, std::list<PathfindingPixelNodeCoordinates>&);
      void constructPathThroughGaseousOrLiquidStopOnVoid(std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>&, std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>::iterator, std::list<PathfindingPixelNodeCoordinates>&);
};

#endif
