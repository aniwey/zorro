#include "Land.hpp"

void Land::loopEntities(){  
  // Entities handling
  std::list<boost::weak_ptr<Entity>>::iterator it = entities.begin();
  while(it != entities.end()){
    if(boost::shared_ptr<Entity> ent = (*it).lock()){
      if(ent->loop(*this)){
        notifyForUpdatingAroundThisPixel(ent->pixelX, ent->pixelY);
        p[ent->pixelX][ent->pixelY].create(*this, ent->pixelX, ent->pixelY, pixelType_NONE);
      }
      it++;
    }
    else{
      it = entities.erase(it);
    }
  }
}
