#include "Land.hpp"

#include <unistd.h>

void Land::loopPixels(){
  checkForGroupsSplittingAndUpdateDependencies();
  resolveDependenciesBetweenGroups();
  applyGravityOnAtu();
  applyGravityOnGroupsOutsideAtu();
  loopDirt();
  loopWater();
  destroyEmptyGroups();
}

void Land::destroyEmptyGroups(){
  // We destroy all groups with no pixels
  std::list<boost::shared_ptr<Group> >::iterator it = g.begin();
  while(it != g.end()){ // Iteration over groups
    if((*it)->hasPixels() == false){ // If the group has no pixels
      (*it).reset(); // Reset the shared pointer
      it = g.erase(it); // Remove it from the list
    }
    else it++; // Increase the iterator if we didn't remove it
  }
}

void Land::applyGravityOnGroupsOutsideAtu(){
  // Apply gravity on group pixels of all groups if gravity wasn't already applied on them just before
  for(std::list<boost::shared_ptr<Group> >::iterator it = g.begin(); it != g.end(); it++){ // Iteration over groups
    if((*it)->hasPixels() && (*it)->canFall()){ // If this group can fall
      bool modif;
      do{
        modif = false;
        for(std::list<boost::shared_ptr<GroupPixel> >::iterator it2 = (*it)->pixels.begin(); it2 != (*it)->pixels.end(); it2++){ // Iteration over pixels in this group to update
          if(p[(*it2)->x][(*it2)->y].feltAtThisFrame < frame_id){ // If this pixel didn't just felt
            // We try to make it fall
            if(tryToMakeFallAlongWithPixelsBelowOrAbove((*it2)->x, (*it2)->y)){
              modif = true;
              break;
            }
          }
        }
      }while(modif == true);
    }
  }
}

void Land::applyGravityOnAtu(){
  int y1, y2;
  
  // Apply gravity on alone pixel & group pixels with no dependencies
  for(unsigned int i = 0; i < atu.size(); ++i){ // Iteration over the columns
    for(std::list<std::pair<int, int> >::iterator it = atu[i].begin(); it != atu[i].end(); it++){ // Iteration over areas in this column
      // We search the correct bottom of the area : it's in fact (*it).first, unless the area begins at the bottom of the land, in which case we take height-1 as the real bottom
      int bottom = (*it).first;
      if(bottom > height-1) bottom = height-1;
      
      // We try to make fall every pixel in the area
      for(int j = bottom; j >= (*it).second; --j){
        // If we manage to make this column fall
        if(tryToMakeFallAlongWithPixelsBelowOrAbove(i, j, y1, y2)){
          j = y1; // j is now the top of the falling column
        }
      }
    }
  }
}

void Land::resolveDependenciesBetweenGroups(){
  // Set the checked bool to false for every group
  for(std::list<boost::shared_ptr<Group> >::iterator it = g.begin(); it != g.end(); it++){ // Iteration over groups
    (*it)->checked = false;
  }
  
  // Resolve dependencies between groups, determine which one can fall and which one can't
  for(std::list<boost::shared_ptr<Group> >::iterator it = g.begin(); it != g.end(); it++){ // Iteration over groups
    (*it)->checked = false;
    if((*it)->hasPixels()) (*it)->resolveDependencies(*this);
  }
}

void Land::checkForGroupsSplittingAndUpdateDependencies(){
  // Set the checked bool to false for every group
  for(std::list<boost::shared_ptr<Group> >::iterator it = g.begin(); it != g.end(); it++){ // Iteration over groups
    (*it)->checked = false;
  }
  
  // Check for splitting all groups
  for(std::list<boost::shared_ptr<Group> >::iterator it = g.begin(); it != g.end(); it++){
    if((*it)->checked == false){
      (*it)->checked = true; // It's checked now
      (*it)->checkForSplitting(*this); // And we check for splitting
    }
  }
  
  /*
  // If there's at least one group
  if(g.size() != 0){
    // Check for splitting groups inside the areas to update
    for(unsigned int i = 0; i < atu.size(); ++i){ // Iteration over the columns
      for(std::list<std::pair<int, int> >::iterator it = atu[i].begin(); it != atu[i].end(); it++){ // Iteration over areas in this column
        for(int j = (*it).first; j >= (*it).second; --j){ // Iteration over pixels in this area
          // If this pixel has a group, we access it
          if(boost::shared_ptr<Group> groupSharedPtr = p[i][j].group.lock()){
            // If this group isn't checked yet
            if(groupSharedPtr->checked == false){
              groupSharedPtr->checked = true; // It's checked now
              groupSharedPtr->checkForSplitting(*this); // And we check for splitting
            }
          }
        }
      }
    }
  }
  */
  
  // Update dependencies of group pixels in the areas to update
  for(unsigned int i = 0; i < atu.size(); ++i){ // Iteration over the columns
    for(std::list<std::pair<int, int> >::iterator it = atu[i].begin(); it != atu[i].end(); it++){ // Iteration over areas in this column
      for(int j = (*it).first; j >= (*it).second; --j){ // Iteration over pixels in this area
        if(boost::shared_ptr<Group> groupSharedPtr = p[i][j].group.lock()){ // If this pixel has a group
          // Get the corresponding group pixel
          boost::shared_ptr<GroupPixel> groupPixelWeAreWorkingOn = groupSharedPtr->getGroupPixelSharedPtr(i, j);
        
          // We save the old dependency
          GroupDependencyType oldDependencyType = groupPixelWeAreWorkingOn->depType;
          
          // We set the default dependency, which will be applied if nothing relevant is found below the pixel
          groupPixelWeAreWorkingOn->depType = GroupDependencyType_BOTTOM_OF_THE_MAP;
          
          // We search below until we find a pixel with a group / which can't fall
          for(int k = j+1; k < height; ++k){
            if(boost::shared_ptr<Group> pixelBelowGroupSharedPtr = p[i][k].group.lock()){ // If this pixel below has a group
              if(groupSharedPtr != pixelBelowGroupSharedPtr){ // If its group is different from ours
                // We get the group pixel corresponding to it and we save it for future operations
                boost::shared_ptr<GroupPixel> groupPixelWeAreGoingToDependOn = pixelBelowGroupSharedPtr->getGroupPixelSharedPtr(i, k);

                // If we were already depending on a pixel
                if(oldDependencyType == GroupDependencyType_GROUP){
                  // And we can access it
                  if(boost::shared_ptr<GroupPixel> pixelWeDependOn = groupPixelWeAreWorkingOn->pixelWeDependOn.lock()){
                    // If the group pixel we were depending on isn't the group pixel we found
                    if(*pixelWeDependOn != GroupPixel(i, k)){
                      // We tell the group pixel we were depending on that no pixel depends on it anymore
                      pixelWeDependOn->aPixelDependsOnUs = false;
                    }
                  }
                }
                
                // We now depend on the new pixel
                groupPixelWeAreWorkingOn->depType = GroupDependencyType_GROUP;
                groupPixelWeAreWorkingOn->pixelWeDependOn = groupPixelWeAreGoingToDependOn;
                
                // If it already has a pixel depending on it
                if(groupPixelWeAreGoingToDependOn->aPixelDependsOnUs == true){
                  // And we can access it
                  if(boost::shared_ptr<GroupPixel> pixelWhichDependsOnTheGroupPixelWeAreGoingToDependOn = groupPixelWeAreGoingToDependOn->pixelWhichDependsOnUs.lock()){
                    // If this pixel depending on it isn't already us
                    if(*pixelWhichDependsOnTheGroupPixelWeAreGoingToDependOn != GroupPixel(i, j)){
                      // We tell the pixel which depends on it that he depends on us now !
                      pixelWhichDependsOnTheGroupPixelWeAreGoingToDependOn->pixelWeDependOn = groupPixelWeAreWorkingOn;
                    }
                  }
                }
                
                // Aaaand we finally tell it that we depend on it now
                groupPixelWeAreGoingToDependOn->aPixelDependsOnUs = true;
                groupPixelWeAreGoingToDependOn->pixelWhichDependsOnUs = groupPixelWeAreWorkingOn;
              }
              else{ // Else, same group as ours, no dependency
                groupPixelWeAreWorkingOn->depType = GroupDependencyType_NOTHING;
              }
              break;
            }
            // Else, if the pixel below can't fall, we set the dependency
            else if(pixelGravityVector[p[i][k].type] == pixelGravity_CANT_FALL){
              groupPixelWeAreWorkingOn->depType = GroupDependencyType_CANT_FALL_PIXEL;
              break;
            }
            // Else, if the pixel below isn't solid
            else if(pixelPhysicalStateVector[p[i][k].type] != pixelPhysicalState_SOLID){
              groupPixelWeAreWorkingOn->depType = GroupDependencyType_NOTHING;
              break;
            }
          }
        }
      }
    }
  }
}

bool Land::tryToMakeFallAlongWithPixelsBelowOrAbove(int x, int y){
  int a, b;
  return tryToMakeFallAlongWithPixelsBelowOrAbove(x, y, a, b);
}

bool Land::tryToMakeFallAlongWithPixelsBelowOrAbove(int x, int y, int& y1, int& y2){
  // Calculate y2 (bottom of the falling column)
  for(y2 = y; y2 < height-1; ++y2){
    // If the pixel has a group
    if(boost::shared_ptr<Group> groupSharedPtr = p[x][y2].group.lock()){
      // ...but this group can't fall
      if(groupSharedPtr->canFall() == false){
        // We break the loop, since this pixel can't fall
        break;
      }
    }
    // Else the pixel has no group
    else{
      // If the pixel may not fall
      if(pixelGravityVector[p[x][y2].type] != pixelGravity_MAY_FALL){
        // We break the loop, since this pixel can't fall
        break;
      }
    }
    // If the pixel already felt this frame
    if(p[x][y2].feltAtThisFrame == frame_id){
      // We break the loop, since this pixel can't fall
      break;
    }
  }
  
  // We set y2 to the previous pixel, which was able to fall
  y2--;
  
  // If the physical state of the pixel at y2+1 isn't solid, we will be able to make fall the whole column, so we continue
  if(pixelPhysicalStateVector[p[x][y2+1].type] != pixelPhysicalState_SOLID){
    // Calculate y1 (top of the falling column)
    for(y1 = y; y1 >= 0; --y1){
      // If the pixel has a group
      if(boost::shared_ptr<Group> groupSharedPtr = p[x][y1].group.lock()){
        // ...but this group can't fall
        if(groupSharedPtr->canFall() == false){
          // We break the loop, since this pixel can't fall
          break;
        }
      }
      // Else the pixel has no group
      else{
        // If the pixel may not fall
        if(pixelGravityVector[p[x][y1].type] != pixelGravity_MAY_FALL){
          // We break the loop, since this pixel can't fall
          break;
        }
      }
      // If the pixel already felt this frame
      if(p[x][y1].feltAtThisFrame == frame_id){
        // We break the loop, since this pixel can't fall
        break;
      }
    }
    
    // We set y1 to the next pixel, which was able to fall
    y1++;
    
    // If we can make fall at least one pixel
    if(y2 >= y1){
      // To know how we will make these pixels fall, we should check the physical state of the pixel at y2+1
      switch(pixelPhysicalStateVector[p[x][y2+1].type]){
        // Void
        case pixelPhysicalState_VOID:
          // We make fall pixels from y2 to y1, it cannot fail
          for(int j = y2; j >= y1; --j){
            makeFall(x, j);
          }
          // Notify
          notifyForUpdatingThisRectangle(x-1, y1-1, x+1, y2+2);
          // Return true
          return true;
        break;
        // Gaseous or liquid
        case pixelPhysicalState_GASEOUS_OR_LIQUID:
          // We make fall pixels from y2 to y1, it cannot fail (notifications are handled by the function we call)
          makeFallThroughAGaseousOrLiquidPixel(x, y1, y2);
          // Return true
          return true;
        break;
        // Anything else
        default: break;
      }
    }
  }
  
  // We didn't make fall any pixel : we return false
  return false;
}

void Land::makeFallThroughAGaseousOrLiquidPixel(int x, int y){
  makeFallThroughAGaseousOrLiquidPixel(x, y, y);
}

void Land::makeFallThroughAGaseousOrLiquidPixel(int x, int y1, int y2){
  // If we're on the top of the map or the pixel above isn't void
  if(y1 == 0 || pixelPhysicalStateVector[p[x][y1-1].type] != pixelPhysicalState_VOID){
    // We make fall all the pixels
    for(int j = y2; j >= y1; --j){
      makeFall(x, j);
    }
    // We notify
    notifyForUpdatingThisRectangle(x-1, y1-1, x+1, y2+2);
  }
  // Else, we use a pathfining algorithm to get out of the way the gaseous or liquid pixel which is blocking us
  else{
    // The list of nodes (pixels) that will need to fall if we succeed
    std::list<PathfindingPixelNodeCoordinates> nodes = std::list<PathfindingPixelNodeCoordinates>();
    
    // Used to store the return value of the AStar algorithm
    aStarStopOnReturnValue aStarAlgorithmReturnValue;

    // We use the AStar algorithm to find the quickest path between (x, y1-1) and (x, y2+1)
    // The coordinates are added to the nodes list in reverse order
    aStarAlgorithmReturnValue = pathfindingThroughGaseousOrLiquidStopOnVoid(PathfindingPixelNodeCoordinates(x, y2+1), PathfindingPixelNodeCoordinates(x, y1), nodes);

    // If we found a path
    if(aStarAlgorithmReturnValue != NO_PATH_FOUND){
      // We add to the list all the pixels we want to make fall (in reverse order)
      for(int i = y2; i >= y1; i--){
        nodes.push_back(PathfindingPixelNodeCoordinates(x, i));
      }
      
      // We swap pixels according to the path found above
      switch(aStarAlgorithmReturnValue){
        // The algorithm found a path without stop (= all the nodes forming a path from y2+1 to y1-1 were added to the list)
        case PATH_FOUND_WITHOUT_STOP:
          // Move all the pixels from the list (the last one will replace the first one). Let's say we have these 8 pixels in the list, number 8 being the last one : 8-7-6-5-4-3-2-1
          // Number 1 is the pixel at y1, number 8 is the pixel at y1-1
          // So we have to swap number 1 and number 8, then number 8 and number 7... and finally number 3 and number 2
          // That's what we do! :)
          makeFall(nodes.back().x, nodes.back().y, nodes.front().x, nodes.front().y); // We begin by swapping number 1 and number 8
          for(std::list<PathfindingPixelNodeCoordinates>::iterator it = nodes.begin(); it != std::prev(nodes.end(), 2); ++it){
            // Swap
            makeFall((*it).x, (*it).y, (*std::next(it, 1)).x, (*std::next(it, 1)).y);
            // Notify
            notifyForUpdatingAroundThisPixel((*it).x, (*it).y);
            notifyForUpdatingAroundThisPixel((*std::next(it, 1)).x, (*std::next(it, 1)).y);
          }
        break;
        // The algorithm found a path with a stop (= all the nodes forming a path from y2+1 to a void pixel somewhere were added to the list)
        case PATH_FOUND_WITH_STOP:
          // Move all the pixels from the list. Let's say we have these 7 pixels in the list, number 7 being the last one : 7-6-5-4-3-2-1
          // Number 7 is the void pixel, number 1 is the pixel at y1
          // Numbers 2, 3, 4, 5, 6 form the path between the pixel at y1 and the void pixel
          // So we have to swap number 7 and number 6, then swap number 6 and number 5, then swap number 5 and number 4, etc
          // That's what we do! :)
          for(std::list<PathfindingPixelNodeCoordinates>::iterator it = nodes.begin(); it != std::prev(nodes.end(), 1); ++it){
            // Swap
            makeFall((*it).x, (*it).y, (*std::next(it, 1)).x, (*std::next(it, 1)).y);
            // Notify
            notifyForUpdatingAroundThisPixel((*it).x, (*it).y);
            notifyForUpdatingAroundThisPixel((*std::next(it, 1)).x, (*std::next(it, 1)).y);
          }
        break;
        default: break;
      }
    }
    // Else, we didn't find a path
    else{
      // We make fall all the pixels
      for(int j = y2; j >= y1; --j){
        makeFall(x, j);
      }
      // We notify
      notifyForUpdatingThisRectangle(x-1, y1-1, x+1, y2+2);
    }
  }
}

void Land::makeFall(int x, int y){
  makeFall(x, y, x, y+1);
}

void Land::makeFall(int x1, int y1, int x2, int y2){
  // We swap with the pixel below
  swap(p[x1][y1], p[x2][y2]);
  
  // We set that both pixels felt at this frame
  p[x1][y1].feltAtThisFrame = frame_id;
  p[x2][y2].feltAtThisFrame = frame_id;
  
  // We tell the pixel that it moved
  p[x2][y2].youJustMovedTo(x2, y2);
  
  // And if we belong to a group..
  if(boost::shared_ptr<Group> groupSharedPtr = p[x2][y2].group.lock()){
    // We unregister the old pixel and register the new one, so that our group can track us
    groupSharedPtr->unregisterPixel(x1, y1, true);
    groupSharedPtr->registerPixel(x2, y2);
  }
}
