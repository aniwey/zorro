#include "Land.hpp"

void Land::loopWater(){
  for(unsigned int i = 0; i < atu.size(); ++i){ // Iteration over the columns
    for(std::list<std::pair<int, int> >::iterator it = atu[i].begin(); it != atu[i].end(); it++){ // Iteration over areas in this column
      for(int j = (*it).first; j >= (*it).second; --j){ // Iteration over pixels in this area
        // If this pixel is water, we try to do something
        if(p[i][j].type == pixelType_WATER && p[i][j].feltAtThisFrame != frame_id){
          // If we're not at the bottom of the map and the pixel below is void, we go down
          if(j < height-1 && pixelPhysicalStateVector[p[i][j+1].type] == pixelPhysicalState_VOID){
            std::swap(p[i][j], p[i][j+1]);
            notifyForUpdatingThisRectangle(i-1, j-1, i+1, j+2); // We notify
          }
          // Else, we didn't make it fall, we try to work on the water's physics
          // If we're not at the top of the map and there's a water pixel above us
          else if(j > 0 && p[i][j-1].type == pixelType_WATER){
            // If we can push ourselves from here
            if(tryToPushThisWaterPixel(i, j)){
              // We move all the water pixels above
              int l = j-1; while(l >= 0 && p[i][l].type == pixelType_WATER){ l--; }
              l++; // Here l is the last water pixel
              // We swap the air pixel at j with the water pixel at l
              std::swap(p[i][j], p[i][l]);
              // We notify around l (the rest of the notifications being handled by the tryToPushThisWaterPixel function)
              notifyForUpdatingThisRectangle(i-1, l-1, i+1, j+1);
              // We continue from l
              j = l;
            }
          }
          // Else, we're not at the bottom of the map
          else if(j < height-1){
            // Try to push ourselves to the center of our water body
            tryToPushThisWaterPixelToTheCenterOfItsWaterBody(i, j);
          }
        }
      }
    }
  }
}

bool Land::tryToPushThisWaterPixel(int x, int y){
  // We try to push the water pixel from its own x position
  return tryToPushThisWaterPixelFromHere(x, y, x);
}

bool Land::tryToPushThisWaterPixelFromHere(int x, int y, int fromHereX){
  bool canPushLeft = true, canPushRight = true;
  
  for(int i = 0; i < width; ++i){
    // If x != fromeHereX (which means we have an ideal position, fromHereX) AND i is superior to the distance between the pixel x position and the pixel ideal position (fromHereX)
    // By the way, we need to use ">=" and not ">" because if we don't, there will be a problem in this kind of situation :
    // AWWWAA (where A is Air and W is Water)
    // 012345
    // If we used ">", the W pixel on 1 wouldn't know where to go (on 1 or on 4?) and would keep moving, therefore keeping notifications busy
    // But when we use ">=", it must go on 1 because it's on the left and the left is first in algorithms
    if(i >= getDistanceBetween(x, fromHereX) && x != fromHereX){
      // It means we're trying to push the pixel further from the ideal position, that's useless, we stop the loop
      break;
    }
    // Try to push to the left
    if(canPushLeft && fromHereX-i >= 0){
      // If this pixel on the left is void
      if(pixelPhysicalStateVector[p[fromHereX-i][y].type] == pixelPhysicalState_VOID){
        // We push to the left
        std::swap(p[x][y], p[fromHereX-i][y]); 
        notifyForUpdatingAroundThisPixel(x, y);
        notifyForUpdatingAroundThisPixel(fromHereX-i, y);
        return true;
      }
      // Else, if this pixel on the left isn't water
      else if(p[fromHereX-i][y].type != pixelType_WATER){
        // We won't be able to push to the left later
        canPushLeft = false;
      }
    }
    // Try to push to the right
    if(canPushRight && fromHereX+i <= width-1){
      // If this pixel on the left is void
      if(pixelPhysicalStateVector[p[fromHereX+i][y].type] == pixelPhysicalState_VOID){
        // We push to the right
        std::swap(p[x][y], p[fromHereX+i][y]); 
        notifyForUpdatingAroundThisPixel(x, y);
        notifyForUpdatingAroundThisPixel(fromHereX+i, y);
        return true;
      }
      // Else, if this pixel on the left isn't water
      else if(p[fromHereX+i][y].type != pixelType_WATER){
        // We won't be able to push to the left later
        canPushRight = false;
      }
    }
    // If we can't push to either left or right, we stop the loop
    if(canPushRight == false && canPushLeft == false)
      return false;
  }
  
  return false;
}

void Land::tryToPushThisWaterPixelToTheCenterOfItsWaterBody(int x, int y){
  int waterBodyLeftLimit, waterBodyRightLimit;
  int waterBodyCenter;
  
  // Find the limits of the water body
  findWaterBodyLimits(x, y, waterBodyLeftLimit, waterBodyRightLimit);
  
  // Calculate the water body center (we use ceil to counterbalance the fact that the left is always first in water algorithms, here with ceil the center will be a little more on the right, statistically)
  waterBodyCenter = waterBodyLeftLimit + std::ceil(((float)waterBodyRightLimit - (float)waterBodyLeftLimit)/2);
  
  // Try to push the water pixel, starting from the water body center, if we're not already in this center
  if(x != waterBodyCenter)
    tryToPushThisWaterPixelFromHere(x, y, waterBodyCenter);
}

void Land::findWaterBodyLimits(int x, int y, int& waterBodyLeftLimit, int& waterBodyRightLimit, int yGap){
  bool waterBodyEndedOnTheLeft = false, waterBodyEndedOnTheRight = false;
  
  // Set the default limit values
  waterBodyLeftLimit = waterBodyRightLimit = x;
  
  // Find the limits of the water body
  for(int i = 1; i < width; ++i){
    // If the water body isn't ended on the left yet
    if(waterBodyEndedOnTheLeft == false){
      // If the water body ends on the left
      if(x-i < 0 || p[x-i][y+yGap].type != pixelType_WATER){
        waterBodyEndedOnTheLeft = true;
        waterBodyLeftLimit = x - i + 1;
      }
    }
    // If the water body isn't ended on the right yet
    if(waterBodyEndedOnTheRight == false){
      // If the water body ends on the right
      if(x+i > width-1 || p[x+i][y+yGap].type != pixelType_WATER){
        waterBodyEndedOnTheRight = true;
        waterBodyRightLimit = x + i - 1;
      }
    }
    // If the water body is ended on the left and the right, we stop the loop
    if(waterBodyEndedOnTheLeft && waterBodyEndedOnTheRight)
      break;
  }
}

bool Land::findWaterBodyLimitsAndReturnTrueIfThereIsAnyVoidPixelAboveTheBody(int x, int y, int& waterBodyLeftLimit, int& waterBodyRightLimit, int yGap){
  bool waterBodyEndedOnTheLeft = false, waterBodyEndedOnTheRight = false;
  bool voidPixelAbove = false;
  
  // Set the default limit values
  waterBodyLeftLimit = waterBodyRightLimit = x;
  
  // Find the limits of the water body
  for(int i = 1; i < width; ++i){
    // If the water body isn't ended on the left yet
    if(waterBodyEndedOnTheLeft == false){
      // If the water body ends on the left
      if(x-i < 0 || p[x-i][y+yGap].type != pixelType_WATER){
        waterBodyEndedOnTheLeft = true;
        waterBodyLeftLimit = x - i + 1;
      }
      // Check for a void pixel above
      else if(voidPixelAbove == false){
        if(y+yGap-1 >= 0 && pixelPhysicalStateVector[p[x-i][y+yGap-1].type] == pixelPhysicalState_VOID)
          voidPixelAbove = true;
      }
    }
    // If the water body isn't ended on the right yet
    if(waterBodyEndedOnTheRight == false){
      // If the water body ends on the right
      if(x+i > width-1 || p[x+i][y+yGap].type != pixelType_WATER){
        waterBodyEndedOnTheRight = true;
        waterBodyRightLimit = x + i - 1;
      }
      // Check for a void pixel above
      else if(voidPixelAbove == false){
        if(y+yGap-1 >= 0 && pixelPhysicalStateVector[p[x+i][y+yGap-1].type] == pixelPhysicalState_VOID)
          voidPixelAbove = true;
      }
    }
    // If the water body is ended on the left and the right, we stop the loop
    if(waterBodyEndedOnTheLeft && waterBodyEndedOnTheRight)
      break;
  }
  
  return voidPixelAbove;
}

