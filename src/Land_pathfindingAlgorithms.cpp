#include "Land.hpp"

aStarStopOnReturnValue Land::pathfindingThroughGaseousOrLiquidStopOnVoid(PathfindingPixelNodeCoordinates startNodeCoordinates, PathfindingPixelNodeCoordinates goalNodeCoordinates, std::list<PathfindingPixelNodeCoordinates>& path){
  // Create the open and closed maps
  std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode> closedMap = std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>(); // Map of nodes already evaluated
  std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode> openMap = std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>(); // Map of nodes to be evaluated
  
  // Create some iterators which will be used later
  std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>::iterator currentNode;
  std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>::iterator neighbor;
  
  // Create the list of neighbors coordinates, which will be used later
  std::list<PathfindingPixelNodeCoordinates> neighborsCoordinatesList;
  
  // Add the start node to the open map (with a score of 0)
  openMap[startNodeCoordinates] = DijkstraPixelNode(0);
 
  // While the open map isn't empty, do the algorithm stuff
  while(openMap.empty() == false){
    // Set the current node to the iterator of the node in the open map having the lowest f score
    currentNode = openMap.begin();
    for(std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>::iterator it = openMap.begin(); it != openMap.end(); ++it){
      if((*it).second.score < (*currentNode).second.score)
        currentNode = it;
    }
    
    // If the current node is a void pixel
    if(pixelPhysicalStateVector[p[(*currentNode).first.x][(*currentNode).first.y].type] == pixelPhysicalState_VOID){
      // Construct the path using the current node (we want to add the void pixel to the path)
      constructPathThroughGaseousOrLiquidStopOnVoid(closedMap, currentNode, path);
      
      // Return
      return PATH_FOUND_WITH_STOP;
    }
    
    // Add the current node to the closed map
    closedMap[(*currentNode).first] = (*currentNode).second;
    
    // The current node is now the one from the closed map
    currentNode = closedMap.find((*currentNode).first);
    
    // Remove the current node from the open map
    openMap.erase((*currentNode).first);

    // Iterate over neighbors of the current node
    neighborsCoordinatesList = getPathfindingNeighborNodesCoordinates((*currentNode).first);
    for(std::list<PathfindingPixelNodeCoordinates>::iterator it = neighborsCoordinatesList.begin(); it != neighborsCoordinatesList.end(); ++it){
      // If this neighbor is actually our goal node
      if((*it) == goalNodeCoordinates){
        // Construct the path using the current node (we don't want to add the goal node to the path)
        constructPathThroughGaseousOrLiquidStopOnVoid(closedMap, currentNode, path);
        
        // Return
        return PATH_FOUND_WITHOUT_STOP;
      }
      
      // If this neighbor is in the closed map
      if(closedMap.count((*it)) > 0){
        // We don't evaluate it and continue with the next neighbor
        continue;
      }
      
      neighbor = openMap.find((*it));
      // If this neighbor is already in the open map
      if(neighbor != openMap.end()){
        // Calculate the new score this neighbor would have if we took ourselves as its parent
        int newScore = (*currentNode).second.score + pixelGaseousOrLiquidPenetrationVector[p[(*it).x][(*it).y].type];
        // If this new score is lower than the current score of this neighbor
        if(newScore < (*neighbor).second.score){
          // Update the score and the parent of this neighbor
          (*neighbor).second.setParentAndScore((*currentNode).first, newScore);
        }
      }
      // Else, this neighbor isn't in the open map
      else{
        // We add it to the open map and set its parent and scores
        openMap[(*it)] = DijkstraPixelNode((*currentNode).first, (*currentNode).second.score + pixelGaseousOrLiquidPenetrationVector[p[(*it).x][(*it).y].type]);
      }
    }
  }
    
  // If we get there, it means we didn't find any path
  return NO_PATH_FOUND;
}

void Land::constructPathThroughGaseousOrLiquidStopOnVoid(std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>& closedMap, std::map<PathfindingPixelNodeCoordinates, DijkstraPixelNode>::iterator node, std::list<PathfindingPixelNodeCoordinates>& path){
  // Boolean used to know when to stop the loop
  bool continueLoop = true;

  while(continueLoop){
    // If the current node isn't a solid pixel (we don't add solid pixels to the path, we "jump" over them
    if(pixelPhysicalStateVector[p[(*node).first.x][(*node).first.y].type] != pixelPhysicalState_SOLID){
      // Add the coordinates of the current node to the path
      path.push_back((*node).first);
    }
    
    // If the current node has a parent
    if((*node).second.parent.x != -1){
      // This parent is now the current node
      node = closedMap.find((*node).second.parent);
    }
    // Else, the current node has no parent, it means it is the starting node
    else{
      // So we stop the loop
      continueLoop = false;
    }
  }
}

std::list<PathfindingPixelNodeCoordinates> Land::getPathfindingNeighborNodesCoordinates(PathfindingPixelNodeCoordinates nodeCoordinates){
  // Create the list
  std::list<PathfindingPixelNodeCoordinates> list = std::list<PathfindingPixelNodeCoordinates>();
  
  // Add neighbor nodes coordinates to the list
  if(nodeCoordinates.x > 0)
    list.push_back(PathfindingPixelNodeCoordinates(nodeCoordinates.x-1, nodeCoordinates.y));
  
  if(nodeCoordinates.x < width-1)
    list.push_back(PathfindingPixelNodeCoordinates(nodeCoordinates.x+1, nodeCoordinates.y));
  
  if(nodeCoordinates.y > 0)
    list.push_back(PathfindingPixelNodeCoordinates(nodeCoordinates.x, nodeCoordinates.y-1));
  
  if(nodeCoordinates.y < height-1)
    list.push_back(PathfindingPixelNodeCoordinates(nodeCoordinates.x, nodeCoordinates.y+1));
  
  // Return the list
  return list;
}
