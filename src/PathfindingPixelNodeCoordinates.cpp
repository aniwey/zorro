#include "PathfindingPixelNodeCoordinates.hpp"

PathfindingPixelNodeCoordinates::PathfindingPixelNodeCoordinates(){
  PathfindingPixelNodeCoordinates(-1, -1);
}

PathfindingPixelNodeCoordinates::PathfindingPixelNodeCoordinates(int _x, int _y){
  x = _x;
  y = _y;
}
