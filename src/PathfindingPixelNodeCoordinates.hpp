#ifndef HPP_PathfindingPixelNodeCoordinates
#define HPP_PathfindingPixelNodeCoordinates

class PathfindingPixelNodeCoordinates{
  public:
    PathfindingPixelNodeCoordinates();
    PathfindingPixelNodeCoordinates(int, int);
    
    // The coordinates
    int x;
    int y;
    
    // Overloading the oprator "==" to be able to compare coordinates
    bool operator==(const PathfindingPixelNodeCoordinates& coord) const{
      return (x == coord.x && y == coord.y);
    };
    
    // Overloading the oprator "!=" to be able to compare coordinates
    bool operator!=(const PathfindingPixelNodeCoordinates& coord) const{
      return (x != coord.x || y != coord.y);
    };
    
    // Ovreloading the operator "<" because we put these coordinates in a map
    bool operator<(const PathfindingPixelNodeCoordinates& coord) const{
      if (x < coord.x) return true;
      if (x > coord.x) return false;
      if (y < coord.y) return true;
      if (y > coord.y) return false;
      
      // Both x and y are equal, we return false
      return false;
    };
};

#endif
