#ifndef HPP_PIXEL
#define HPP_PIXEL

#include <algorithm>
#include <iostream>

//#include <boost/serialization/shared_ptr.hpp>
//#include <boost/serialization/weak_ptr.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <SFML/Graphics/Color.hpp>

#include "Entity.hpp"
#include "Fruit.hpp"
#include "Group.hpp"
#include "pixelType.hpp"
#include "random.hpp"
#include "Seed.hpp"

class Pixel{
  public:
    Pixel();
    ~Pixel();
    
    friend void swap(Pixel& a, Pixel& b){
      std::swap(a.type, b.type);
      std::swap(a.color, b.color);
      swap(a.group, b.group);
      std::swap(a.feltAtThisFrame, b.feltAtThisFrame);
      std::swap(a.entity, b.entity);
    }
    
    // Pixel creation
    void create(Land&, int, int, pixelType);
    void addEntity(Land&, int, int);
    void setColorBasedOnType();
    
    void resetEntityPointer();
    
    // Other functions
    void setColor(int, int, int);
    void removeFromGroup(int, int);
    
    // Callbacks
    void youJustMovedTo(int, int);
    
    // Variables
    pixelType type; // Type of the pixel
    Color color; // Color of the pixel
    boost::weak_ptr<Group> group; // Group of the pixel (optional)
    boost::shared_ptr<Entity> entity; // Pointer towards the entity the pixel is attached to (optional)
    int feltAtThisFrame; // Contains the id of the frame at which the pixel felt for the last time
  
  private:
    // Serialization
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int){
      ar & type;
      ar & color;
      ar & group;
      ar & entity;
      ar & feltAtThisFrame;
    }
  
    // Private functions used by create() method
    bool createEntity(pixelType, Land&, int, int); // Return true if an entity was created
};

#endif
