#include "Screen.hpp"

void Screen::prepareDrawing(){
  window.clear();
}

void Screen::draw(Land& l, God& g){
  prepareDrawing();
  drawLand(l);
  drawCursor(g);
  drawTextInfo();
  endDrawing();
}

void Screen::drawTextInfo(){
  useInterfaceView();
  
  // Update fpsText's string if needed
  if(fpsClockStep == 9){
    fpsText.setString(SCREEN_TEXTINFO + boost::lexical_cast<std::string>((int)(1/(fpsClock.restart().asSeconds()/10))));
    fpsClockStep = 0;
  }
  else fpsClockStep++;
  
  window.draw(fpsText);

  useNormalView();
}

void Screen::drawLand(Land& l){
  writeLandToImage(l);
  writeImageToTexture();
  writeTextureToWindow();
}

void Screen::drawCursor(God& g){
  drawNormalCursor(g);
}

void Screen::drawNormalCursor(God& g){
  float outline, size, gap;
  
  // Calculate the outline : it depends on the view zoom, the smaller viewZoom is, the smaller outline will be
  outline = viewZoom;
  
  // Calculate the size (with borders)
  size = g.getAddingDiameterDependingOnPixelTypeSelected();
  
  // Calculate the gap we need to substract from currentLandCursorPosition in order to draw the cursor at the correct place
  // It must take in account parity of pixelAddingDiameter (which explains the % 2)
  if(size - outline*2 < 1){ // If the rect should be smaller than the pixels we add
    size = 1 + outline*2;
    gap = (size-1) / 2;
  }
  else{
    if((int)size % 2 == 1) gap = (size-1) / 2;
    else gap = size/2;
  }
  
  // Substract the border from the size
  size -= outline*2;
  
  // Draw the point inside the rectangle
  sf::Vertex vertices[] = { sf::Vertex(cursorPosition, sf::Color(0, 0, 0)) };
  window.draw(vertices, 1, sf::Points);
  
  // Draw the rectangle around the cursor
  sf::RectangleShape rect(sf::Vector2f(size, size));
  rect.setPosition(landCursorPosition.x - gap + outline, landCursorPosition.y - gap + outline);
  rect.setFillColor(sf::Color(0, 0, 0, 0));
  rect.setOutlineColor(sf::Color(0, 0, 0));
  rect.setOutlineThickness(outline);
  window.draw(rect);
    
  // Draw another cursor if the mouse is out of land's image
  if(isMouseOutOfImage()){
    sf::Vertex vertices[] = { sf::Vertex(sf::Vector2f(cursorPosition.x - 5*viewZoom, cursorPosition.y - 5*viewZoom), sf::Color(255, 255, 255)),
                            sf::Vertex(sf::Vector2f(cursorPosition.x + 5*viewZoom, cursorPosition.y + 5*viewZoom), sf::Color(255, 255, 255)),
                            sf::Vertex(sf::Vector2f(cursorPosition.x + 5*viewZoom, cursorPosition.y - 5*viewZoom), sf::Color(255, 255, 255)),
                            sf::Vertex(sf::Vector2f(cursorPosition.x - 5*viewZoom, cursorPosition.y + 5*viewZoom), sf::Color(255, 255, 255)) };
    window.draw(vertices, 4, sf::Lines);
  }
}

void Screen::endDrawing(){
  window.display();
}

