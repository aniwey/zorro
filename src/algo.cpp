#include "algo.hpp"

#include <iostream>

int getDistanceBetween(int a, int b){
  return std::abs(a - b);
}

Color getMidColor(Color col1, Color col2, float gradient){
  return Color(getMidInteger(col1.getR(), col2.getR(), gradient), getMidInteger(col1.getG(), col2.getG(), gradient), getMidInteger(col1.getB(), col2.getB(), gradient));
}

int getMidInteger(int a, int b, float gradient){
  return std::min(a, b) + abs(a-b)*gradient;
}
