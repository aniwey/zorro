#ifndef HPP_ALGO
#define HPP_ALGO

#include <algorithm>

#include "Color.hpp"

int getDistanceBetween(int, int);

Color getMidColor(Color, Color, float);
int getMidInteger(int, int, float);

#endif
