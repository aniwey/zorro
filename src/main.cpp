#include "ConfigData.hpp"
#include "God.hpp"
#include "Land.hpp"
#include "random.hpp"
#include "Screen.hpp"

#include <iostream>

int main(void){
  // Create ConfigData objects and load configuration files
  ConfigData mainConfig;
  mainConfig.readFromFile("./../config/cfg.txt");
  ConfigData textStrings;
  textStrings.readFromFile("./../config/localization/" + mainConfig.getString("language") + ".txt");
  
  // Create the screen object
  Screen s;
  s.init(mainConfig);
  
  // Create the god object
  God g;
  
  // Create the land object
  Land l;
  l.newGame(s.getWidth()/2 - 20, s.getHeight()/2 - 40);
  s.adaptToLand(l, true);
  
  // Other stuff
  bool exit = 0;
  initRandom();
  
  while(!exit){
    l.handleNotifications();
    s.draw(l, g);
    l.loop();
    s.handleEvents(l, g, exit);
  }
  
  return 0;
}
