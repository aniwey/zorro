#ifndef HPP_PIXELTYPE
#define HPP_PIXELTYPE

typedef enum{
  pixelType_INVALID,
  pixelType_NONE,
  pixelType_DIRT,
  pixelType_STONE,
  pixelType_SEED,
  pixelType_LEAVES,
  pixelType_FRUIT,
  pixelType_WATER,
  pixelType_TOTAL
}pixelType;

typedef enum{
  pixelGravity_CANT_FALL,
  pixelGravity_MAY_FALL,
  pixelGravity_NOT_RELEVANT
}pixelGravity;

typedef enum{
  pixelPhysicalState_VOID,
  pixelPhysicalState_GASEOUS_OR_LIQUID,
  pixelPhysicalState_SOLID,
  pixelPhysicalState_TOTAL
}pixelPhysicalState;

#endif
